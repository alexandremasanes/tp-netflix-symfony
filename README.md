# Netflix/Needflix

Une application de type Netflix développée en Symfony

## Getting Started

Instructions pour avoir une copie du projet et le faire tourner en local

### Prérequis

Ce dont on a besoin :

- Docker installé sur sa machine afin de pouvoir faire marcher un environnement
- Git (facultatif)
- Composer (facultatif)

### Installation et accès

Récupérer le projet dans un dossier en le téléchargeant directement du repositery
ou éxecuter la commande :

```
git clone https://gitlab.com/aleksbenmaza/tp-netflix-symfony.git
```

A la racine du projet, éxécuter :

```
docker-compose up -d
```

Pour générer le vendor, se positionner dans le dossier /Needflix et faire un :

```
php composer.phar update
```

ou si composer est installé, faire un :

```
composer update
```

Il faut ensuite changer le fichier parameters.yml comme suit :

```
# This file is auto-generated during the composer install
parameters:
    database_host: db
    database_port: null
    database_name: netflix
    database_user: root
    database_password: root
    mailer_transport: smtp
    mailer_host: 127.0.0.1
    mailer_user: null
    mailer_password: null
    secret: 555ffb682316d4b70e8901d9256290a58a81fc5a
```

Pour changer les fixtures, rechanger les parametres en :

```
# This file is auto-generated during the composer install
parameters:
    database_host: 127.0.0.1
    database_port: 3307
    database_name: netflix
    database_user: root
    database_password: root
    mailer_transport: smtp
    mailer_host: 127.0.0.1
    mailer_user: null
    mailer_password: null
    secret: 555ffb682316d4b70e8901d9256290a58a81fc5a
```

Pour accéder à la partie administrateur, il faut ajouter dans l'URL :

```
/admin
```

## Built With

* [Symfony](http://symfony.com/) - Framework utilisé
* [Nginx](https://nginx.org/en/) - Dependency Management
* [Netflix](http://www.netflix.com/) - Modele


## Versioning

Gitlab

## Auteurs

* **Alexandre MASANES** - (https://gitlab.com/aleksbenmaza)
* **Arnaud ADON** - (https://github.com/Arnaud-Adon)
* **Warren A. MADHOW R.** - (https://github.com/DyniW)
