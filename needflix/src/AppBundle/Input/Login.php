<?php

namespace AppBundle\Input;

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 26/02/2018
 * Time: 10:03
 */
class Login {

    /**
     * @var string
     */
    private $email_address;

    /**
     * @var string
     */
    private $password;

    /**
     * @return string|null
     */
    public function getEmailAddress(): ? string {
        return $this->email_address;
    }

    /**
     * @param string|null $email_address
     */
    public function setEmailAddress(? string $email_address): void {
        $this->email_address = $email_address;
    }

    /**
     * @return string|null
     */
    public function getPassword(): string {
        return $this->password;
    }

    /**
     * @param string|null $password|null
     */
    public function setPassword(? string $password): void {
        $this->password = $password;
    }
}