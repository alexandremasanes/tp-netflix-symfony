<?php

namespace AppBundle\Input;

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 26/02/2018
 * Time: 10:03
 */
class FilmInput {
    /**
     * @var int $category_id
     */
    private $category_id;
    /**
     * @var string $director
     */
    private $director;
    /**
     * @var string $producer
     */
    private $producer;
    /**
     * @var DateTime $release_date
     */
    private $release_date;
    /**
     * @var int $age_restriction
     */
    private $age_restriction;
    /**
     * @var string $description
     */
    private $description;

    /**
     * @return int|null
     */
    public function getCategoryId(): ? int {
        return $this->category_id;
    }

    /**
     * @param int|null $category_id
     */
    public function setCategoryId(? int $category_id): void {
        $this->category_id = $category_id;
    }

    /**
     * @return string|null
     */
    public function getDirector(): ? string {
        return $this->director;
    }

    /**
     * @param string|null $director
     */
    public function setDirector(? string $director): void {
        $this->director = $director;
    }

    /**
     * @return string|null
     */
    public function getProducer(): ? string {
        return $this->producer;
    }

    /**
     * @param string|null $producer
     */
    public function setProducer(? string $producer): void {
        $this->producer = $producer;
    }

    /**
     * @return DateTime|null
     */
    public function getReleaseDate(): ? DateTime {
        return $this->release_date;
    }

    /**
     * @param DateTime|null $release_date
     */
    public function setReleaseDate(? DateTime $release_date): void
    {
        $this->release_date = $release_date;
    }

    /**
     * @return int|null
     */
    public function getAgeRestriction(): ? int
    {
        return $this->age_restriction;
    }

    /**
     * @param int|null $age_restriction
     */
    public function setAgeRestriction(? int $age_restriction): void
    {
        $this->age_restriction = $age_restriction;
    }

    /**
     * @return string
     */
    public function getDescription(): ? string {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(? string $description): void {
        $this->description = $description;
    }
}