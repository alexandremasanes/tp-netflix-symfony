<?php
/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 28/02/2018
 * Time: 14:55
 */

namespace AppBundle\Form;

use AppBundle\Input\Registration;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Registration::class,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $option): void
    {

        $builder->add('first_name', TextType::class, ['attr' => ['placeholder' => 'Prénom', 'class' => 'form-control']])
                ->add('last_name', TextType::class, ['attr' => ['placeholder' => 'Nom']])
                ->add('email_address', EmailType::class, ['attr' => ['placeholder' => 'Address mail', 'class' => 'form-control']])
                ->add('password', PasswordType::class, ['attr' => ['label' => 'Mot de passe', 'class' => 'form-control']])
                ->add('birth_date', DateType::class, ['attr' => ['placeholder' => 'Date de naissance', 'class' => 'form-control']])
                ->add('submit', SubmitType::class, ['attr' => ['value' => 'Inscription', 'class' => 'form-control']]);
    }
}