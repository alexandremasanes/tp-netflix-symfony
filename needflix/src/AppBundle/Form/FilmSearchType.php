<?php
/**
 * Created by PhpStorm.
 * User: Arnaud
 * Date: 23/02/2018
 * Time: 16:43
 */

namespace AppBundle\Form;

use AppBundle\Entity\Category;
use AppBundle\Input\FilmSearch;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class FilmSearchType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class, ['label' => 'Recherche', 'attr' => ['placeholder' => 'entrez le nom d\'un film', 'class' => 'form-control']])
            ->add('submit', SubmitType::class, ['attr' => ['value' => 'Rechercher', 'class' => 'form-control']]);

    }
}