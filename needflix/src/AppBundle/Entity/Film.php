<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Film
 *
 * @ORM\Table(name="films")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FilmRepository")
 */
class Film
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="director", type="string", length=255)
     */
    private $director;

    /**
     * @var string
     *
     * @ORM\Column(name="producer", type="string", length=255)
     */
    private $producer;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="release_date", type="datetime")
     */
    private $releaseDate;

    /**
     * @var int
     *
     * @ORM\Column(name="age_restriction", type="integer")
     */
    private $ageRestriction;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @ORM\OneToOne(targetEntity="Image", cascade={"ALL"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $image;

    /**
     * @var Category
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="films", fetch="EAGER", cascade={"ALL"})
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", nullable=false)
     */
    private $category;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="film", orphanRemoval=true, cascade={"ALL"})
     */
    private $comments;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="FilmToUser", mappedBy="film", orphanRemoval=true, cascade={"ALL"})
     */
    private $filmsToUsers;

    /**
     * Film constructor.
     * @param Category $category
     */

    public function __construct(Category $category = null)
    {
        $this->category = $category;
        $this->filmsToUsers = new ArrayCollection;
        $this->comments = new ArrayCollection;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return void
     */
    public function setTitle(? string $title): void
    {
        $this->title = $title;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle(): ? string
    {
        return $this->title;
    }

    /**
     * Set director
     *
     * @param string $director
     *
     * @return void
     */
    public function setDirector(? string $director): void
    {
        $this->director = $director;
    }

    /**
     * Get director
     *
     * @return string
     */
    public function getDirector(): ? string
    {
        return $this->director;
    }

    /**
     * Set producer
     *
     * @param string $producer
     *
     * @return void
     */
    public function setProducer(? string $producer): void
    {
        $this->producer = $producer;
    }

    /**
     * Get producer
     *
     * @return string
     */
    public function getProducer(): ? string
    {
        return $this->producer;
    }

    /**
     * Set releaseDate
     *
     * @param \DateTime $releaseDate
     *
     * @return void
     */
    public function setReleaseDate(? \DateTime $releaseDate): void {
        $this->releaseDate = $releaseDate;
    }

    /**
     * Get releaseDate
     *
     * @return \DateTime
     */
    public function getReleaseDate(): ? \DateTime
    {
        return $this->releaseDate;
    }

    /**
     * Set ageRestriction
     *
     * @param integer $ageRestriction
     *
     * @return void
     */
    public function setAgeRestriction(? int $ageRestriction): void
    {
        $this->ageRestriction = $ageRestriction;
    }

    /**
     * Get ageRestriction
     *
     * @return int
     */
    public function getAgeRestriction(): ? int
    {
        return $this->ageRestriction;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return void
     */
    public function setDescription(? string $description): void
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription(): ? string
    {
        return $this->description;
    }

    /**
     * @return Category
     */


    public function getCategory(): Category
    {
        return $this->category;
    }

    /**
     * @return ArrayCollection
     */
    public function getComments(): ArrayCollection
    {
        return $this->comments;
    }

    /**
     * @param Comment $comment
     * @return bool
     */
    public function addComment(Comment $comment): bool
    {
        return $this->comments->add($comment);
    }

    /**
     * @param FilmToUser $filmToUser
     * @return bool
     */
    public function addFilmToUser(FilmToUser $filmToUser): bool
    {
        return $this->filmsToUsers->add($filmToUser);
    }

    /**
     * @return ArrayCollection
     */
    public function getFilmsToUsers(): ArrayCollection
    {
        return $this->filmsToUsers;
    }

    /**
     * Set category.
     *
     * @param \AppBundle\Entity\Category $category
     *
     * @return Film
     */
    public function setCategory(\AppBundle\Entity\Category $category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Remove comment.
     *
     * @param \AppBundle\Entity\Comment $comment
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeComment(\AppBundle\Entity\Comment $comment)
    {
        return $this->comments->removeElement($comment);
    }

    /**
     * Add filmsToUser.
     *
     * @param \AppBundle\Entity\FilmToUser $filmsToUser
     *
     * @return Film
     */
    public function addFilmsToUser(\AppBundle\Entity\FilmToUser $filmsToUser)
    {
        $this->filmsToUsers[] = $filmsToUser;

        return $this;
    }

    /**
     * Remove filmsToUser.
     *
     * @param \AppBundle\Entity\FilmToUser $filmsToUser
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeFilmsToUser(\AppBundle\Entity\FilmToUser $filmsToUser)
    {
        return $this->filmsToUsers->removeElement($filmsToUser);
    }

    /**
     * Set image.
     *
     * @param \AppBundle\Entity\Image $image
     *
     * @return Film
     */
    public function setImage(\AppBundle\Entity\Image $image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image.
     *
     * @return \AppBundle\Entity\Image
     */
    public function getImage()
    {
        return $this->image;
    }
}
