<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Comment
 *
 * @ORM\Table(name="comments")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CommentRepository")
 */
class Comment
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="submission_date", type="datetime")
     */
    private $submissionDate;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     */
    private $content;

    /**
     * @var Film
     *
     * @ORM\ManyToOne(targetEntity="Film", inversedBy="comments", fetch="EAGER")
     * @ORM\JoinColumn(name="film_id", referencedColumnName="id", nullable=false)
     */
    private $film;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="comments", fetch="EAGER")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private $user;

    /**
     * Comment constructor.
     * @param User $user
     */
    public function __construct(Film $film, User $user)
    {
        $this->film = $film;
        $this->user = $user;
        $this->comments = new ArrayCollection;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Film
     */
    public function getFilm(): Film
    {
        return $this->film;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * Set dateTime
     *
     * @param null|\DateTime $submissionDate
     */
    public function setSubmissionDate(? \DateTime $submissionDate): void
    {
        $this->submissionDate = $submissionDate;
    }

    /**
     * Get dateTime
     * @return null|\DateTime
     */
    public function getSubmissionDate(): ? \DateTime
    {
        return $this->submissionDate;
    }

    /**
     * Set content
     *
     * @param null|string $content
     */
    public function setContent(? string $content): void
    {
        $this->content = $content;
    }

    /**
     * Get content
     *
     * @return null|string
     */
    public function getContent(): ? string
    {
        return $this->content;
    }

    /**
     * Set film.
     *
     * @param \AppBundle\Entity\Film $film
     *
     * @return Comment
     */
    public function setFilm(\AppBundle\Entity\Film $film)
    {
        $this->film = $film;

        return $this;
    }

    /**
     * Set user.
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Comment
     */
    public function setUser(\AppBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }
}
