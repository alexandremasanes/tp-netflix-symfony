<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * User
 *
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User implements UserInterface
{

    public const INDEX_ROLE_ADMIN = 0;

    public const INDEX_ROLE_USER  = 1;

    public const ROLES = [
        "ADMIN",
        "USER"
    ];

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255)
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=255)
     */
    private $firstName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birth_date", type="datetime")
     */
    private $birthDate;

    /**
     * @var string
     *
     * @ORM\Column(name="email_address", type="string", unique=true)
     */
    private $emailAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="string")
     */
    private $salt;

    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string")
     */
    private $hash;

    /**
     * @var int
     *
     * @ORM\Column(name="role_index", type="integer", nullable=false)
     */
    private $roleIndex;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="user", orphanRemoval=true, cascade={"ALL"})
     */
    private $comments;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="FilmToUser", mappedBy="user", orphanRemoval=true, cascade={"ALL"})
     */
    private $filmsToUsers;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->filmsToUsers = new ArrayCollection;
        $this->comments     = new ArrayCollection;
    }


    /**
     * {@inheritdoc}
     */
    public function getRoles()
    {
        return [
            self::ROLES[
                $this->roleIndex
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getPassword()
    {
        return $this->hash;
    }

    /**
     * {@inheritdoc}
     */
    public function getSalt(): ? string
    {
        return $this->salt;
    }

    /**
     * {@inheritdoc}
     */
    public function getUsername(): ? string
    {
        return $this->emailAddress;
    }

    /**
     * {@inheritdoc}
     */
    public function eraseCredentials() {}

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return void
     */
    public function setLastName(? string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName(): ? string
    {
        return $this->lastName;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return void
     */
    public function setFirstName(? string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName(): ? string
    {
        return $this->firstName;
    }

    /**
     * Set birthDate
     *
     * @param \DateTime $birthDate
     *
     * @return void
     */
    public function setBirthDate(? \DateTime $birthDate): void
    {
        $this->birthDate = $birthDate;
    }

    /**
     * Get birthDate
     *
     * @return \DateTime
     */
    public function getBirthDate(): ? \DateTime
    {
        return $this->birthDate;
    }

    /**
     * @param FilmToUser $filmToUser
     * @return bool
     */
    public function addFilmToUser(FilmToUser $filmToUser): bool
    {
        return $this->filmsToUsers->add($filmToUser);
    }

    /**
     * @return ArrayCollection
     */
    public function getFilmsToUsers(): ArrayCollection
    {
        return $this->filmsToUsers;
    }

    /**
     * @return string|null
     */
    public function getEmailAddress(): ? string
    {
        return $this->emailAddress;
    }

    /**
     * @param string|null $emailAddress
     */
    public function setEmailAddress(? string $emailAddress): void
    {
        $this->emailAddress = $emailAddress;
    }

    /**
     * @return string|null
     */
    public function getHash(): ? string
    {
        return $this->hash;
    }

    /**
     * @param string|null $hash
     */
    public function setHash(? string $hash): void
    {
        $this->hash = $hash;
    }

    /**
     * @return int|null
     */
    public function getRoleIndex(): ? int
    {
        return $this->roleIndex;
    }

    /**
     * @param int|null $roleIndex
     */
    public function setRoleIndex(? int $roleIndex): void
    {
        $this->roleIndex = $roleIndex;
    }

    /**
     * @return ArrayCollection
     */
    public function getComments(): ArrayCollection
    {
        return $this->comments;
    }

    /**
     * @param Comment $comment
     * @return bool
     */
    public function addComment(Comment $comment): bool
    {
        return $this->comments->add($comment);
    }

    /**
     * Set salt.
     *
     * @param string $salt
     *
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Remove comment.
     *
     * @param \AppBundle\Entity\Comment $comment
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeComment(\AppBundle\Entity\Comment $comment)
    {
        return $this->comments->removeElement($comment);
    }

    /**
     * Add filmsToUser.
     *
     * @param \AppBundle\Entity\FilmToUser $filmsToUser
     *
     * @return User
     */
    public function addFilmsToUser(\AppBundle\Entity\FilmToUser $filmsToUser)
    {
        $this->filmsToUsers[] = $filmsToUser;

        return $this;
    }

    /**
     * Remove filmsToUser.
     *
     * @param \AppBundle\Entity\FilmToUser $filmsToUser
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeFilmsToUser(\AppBundle\Entity\FilmToUser $filmsToUser)
    {
        return $this->filmsToUsers->removeElement($filmsToUser);
    }
}
