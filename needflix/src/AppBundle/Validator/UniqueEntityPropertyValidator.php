<?php
/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 01/03/2018
 * Time: 09:40
 */

namespace AppBundle\Validator;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UniqueEntityPropertyValidator extends ConstraintValidator {

    private const VIOLATION_MESSAGE_FORMAT = '%s::$%s value must be unique : %s';

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager) {
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint) {

        if(!$this->entityManager->getClassMetadata($constraint->entityClass))
            throw new \InvalidArgumentException(sprintf("% is not registered as an entity", $constraint->entityClass));

        $exists = $this->entityManager
                       ->getRepository(
                            $constraint->entityClass
                       )
                       ->findOneBy([$constraint->propertyName => $value]);

        if($exists) {
            if(is_string($value))
                $value = '"' . $value . '"';

            $this->context->addViolation(
                sprintf(
                    self::VIOLATION_MESSAGE_FORMAT,
                    $constraint->entityClass,
                    $constraint->propertyName,
                    $value
                )
            );
        }
    }
}