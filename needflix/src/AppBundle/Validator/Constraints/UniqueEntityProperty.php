<?php
/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 28/02/2018
 * Time: 17:09
 */

namespace AppBundle\Validator\Constraints;

use AppBundle\Validator\UniqueEntityPropertyValidator;
use Symfony\Component\Validator\Constraint;

/**
 * Class Unique
 * @package AppBundle\Validator\Constraints
 * @Annotation
 */
class UniqueEntityProperty extends Constraint {

    /**
     * @var string
     */
    public $propertyName;

    /**
     * @var string
     */
    public $entityClass;

    public function validatedBy() {
        return UniqueEntityPropertyValidator::class;
    }


}