<?php

namespace AppBundle\Service;

use AppBundle\Entity\Category;
use AppBundle\Entity\Film;
use AppBundle\Entity\Comment;
use AppBundle\Input\FilmSearch;
use AppBundle\Repository\CategoryRepository;
use AppBundle\Repository\CommentRepository;
use AppBundle\Repository\FilmRepository;

use Doctrine\ORM\EntityManagerInterface;


/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 26/02/2018
 * Time: 10:00
 */
class FilmService {

    /**
     * @var FilmRepository $film_repository
     */
    private $film_repository;
    /**
     * @var CategoryRepository $category_repository
     */
    private $category_repository;
    /**
     * @var CommentRepository $comment_repository
     */
    private $comment_repository;

    /**
     * FilmService constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->film_repository     = $entityManager->getRepository(Film::class);
        $this->category_repository = $entityManager->getRepository(Category::class);
        $this->comment_repository  = $entityManager->getRepository(Comment::class);
    }

    /**
     * @return Film[]
     */
    public function getAllFilms(): array
    {
        return $this->film_repository->findAll();
    }

    /**
     * @param int $id
     * @return null|Film
     */
    public function getFilmById(int $id): ? Film
    {
        return $this->film_repository->find($id);
    }

    /**
     * @param int $category_id
     * @return Film[]
     */
    public function getFilmsByCategory(int $category_id): array
    {
        return $this->film_repository->findBy(["category.id" => $category_id]);
    }

    /**
     * @return FilmRepository\FilmAndAvgRating[]
     */
    public function getFilmsAndAvgRatings(): array
    {
        return $this->film_repository->findAllFilmsAndAvgRatings();
    }

    /**
     * @param int $film_id
     * @return null|FilmRepository\FilmAndAvgRating
     */
    public function getFilmAndAvgRating(int $film_id): ? FilmRepository\FilmAndAvgRating
    {
        return $this->film_repository->findOneFilmAndAvgRating($film_id);
    }

    /**
     * @param FilmSearch $filmSearch
     * @return Film[]
     */
    public function getFilmsBySearch(FilmSearch $filmSearch): array
    {
        return $this->film_repository->findBySearch(
                     $filmSearch->getNameLike(),
                     $filmSearch->getProducer(),
                     $filmSearch->getDirector(),
                     $filmSearch->getReleaseYear(),
                     $filmSearch->getCategoryIds(),
                     $filmSearch->getRestrictionAge(),
                     $filmSearch->getMinimumRating()
        );
    }

    /**
     * @param int $film_id
     * @return Comment[]
     */
    public function getCommentsByFilm(int $film_id): array
    {
        return $this->comment_repository->findBy(["film.id" => $film_id]);
    }

    /**
     * @param int $user_id
     * @return Film[]
     */
    public function getUserFavouriteFilms(int $user_id): array
    {
        return $this->film_repository->findUserFavouriteFilms($user_id);
    }
}