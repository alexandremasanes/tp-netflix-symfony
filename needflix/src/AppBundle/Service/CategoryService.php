<?php
/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 02/03/2018
 * Time: 12:54
 */

namespace AppBundle\Service;


use AppBundle\Entity\Category;
use AppBundle\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;

class CategoryService
{
    /**
     * @var CategoryRepository
     */
    private $category_repository;

    /**
     * CategoryService constructor.
     * @param EntityManagerInterface $entity_manager
     */
    public function __construct(EntityManagerInterface $entity_manager)
    {
        $this->category_repository = $entity_manager->getRepository(Category::class);
    }

    /**
     * @return Category[]
     */
    public function getAllCategories(): array {
        return $this->category_repository->findAll();
    }
}