<?php

namespace AppBundle\Repository\FilmRepository;

use AppBundle\Entity\Film;

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 28/02/2018
 * Time: 11:47
 */
interface FilmAndAvgRating {

    /**
     * @return Film
     */
    public function getFilm(): Film;

    /**
     * @return float
     */
    public function getAverageRating(): float;
}