<?php
/**
 * Created by PhpStorm.
 * User: Arnaud
 * Date: 23/02/2018
 * Time: 13:58
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Film;
use AppBundle\Form\FilmSearchType;
use AppBundle\Service\FilmService;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class FilmController extends Controller
{

    /**
     * @var FilmService
     */
    private $filmService;

    /**
     * FilmController constructor.
     * @param FilmService $filmService
     */
    public function __construct(FilmService $filmService) {
        $this->filmService = $filmService;
    }

    /**
     * @Route("/", name="homepage", methods={"GET"})
     */
    public function indexAction(Request $request)
    {
        $films = $this->filmService->getAllFilms();

        return $this->render('films/index.html.twig', ['films' => $films]);
    }

    /**
     * @Route("/film/view/{id}", name="film_view", methods={"GET"})
     */
    public function viewAction(Session $session, $id)
    {
        $film = $this->filmService->getFilmById($id);

        if (!$film) {
            $session->getFlashBag()->add('error', 'Le film , n\'existe pas');
            return $this->redirectToRoute('homepage');
        }
        return $this->render('films/view.html.twig', ['film' => $film]);
    }


    public function searchFormAction()
    {
        $form = $this->createForm(FilmSearchType::class);
        return $this->render('films/module/search.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/film/search", name="search_film")
     */


    public function searchAction(Request $request){

        $form = $this->createForm(FilmSearchType::class);

        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            $name = $form['title']->getData();
            $em = $this->getDoctrine()->getManager();
            $films = $em->getRepository(Film::class)->search($name);
            dump($films);
            if (!$films) {
                //$session->getFlashBag()->add('error', 'Le film n\'existe pas');
                return $this->redirectToRoute('homepage');
            } else {
                return $this->render('films/searchResult.html.twig', ['films' => $films]);
            }
        }
    }
}