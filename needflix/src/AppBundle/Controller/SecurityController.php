<?php
/**
 * Created by PhpStorm.
 * User: Arnaud
 * Date: 28/02/2018
 * Time: 09:23
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends Controller
{
    /**
     * @var AuthenticationUtils
     */
    private $authenticationUtils;

    /**
     * SecurityController constructor.
     * @param AuthenticationUtils $authenticationUtils
     */
    public function __construct(AuthenticationUtils $authenticationUtils)
    {
        $this->authenticationUtils = $authenticationUtils;
    }

    /**
     * @Security(expression="has_role('IS_AUTHENTICATED_ANONYMOUSLY')")
     * @Route("/login", name="login", methods={"GET"})
     * @Template("login/index.html.twig")
     */
    public function loginAction(): array
    {
        $error = $this->authenticationUtils->getLastAuthenticationError();
        $lastUsername = $this->authenticationUtils->getLastUsername();

        return [
            'error' => $error,
            'last_username' =>$lastUsername
        ];
    }

    /**
     * @Security(expression="is_granted('IS_AUTHENTICATED_REMEMBERED')")
     * @Route("/logout", name="logout", methods={"POST"})
     */
    public function logoutAction() {}
}